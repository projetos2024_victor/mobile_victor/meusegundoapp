import { StatusBar } from "expo-status-bar";
import { StyleSheet, Text, View, ScrollView } from "react-native";
import Layout from "./LayoutDeTelaEstrutura";
import LayoutHorizontal from "./LayoutHorizontal";
import LayoutGrade from "./LayoutGrade";
import Components from "./Components";

export default function App() {
  return (
    <View style={styles.container}>
      <Components></Components>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection:"column",
    justifyContent: "space-between",

  },
});